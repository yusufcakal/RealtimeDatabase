package yusufcakal.realtime_database.pojo;

import java.util.List;

/**
 * Created by Yusuf on 18.01.2018.
 */

public class Book {

    private String name;
    private List<Author> authorList;

    public Book(String name, List<Author> authorList) {
        this.name = name;
        this.authorList = authorList;
    }

    public Book() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", authorList=" + authorList +
                '}';
    }
}
