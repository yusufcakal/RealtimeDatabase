package yusufcakal.realtime_database.pojo;

/**
 * Created by Yusuf on 18.01.2018.
 */

public class Author {

    private String name;

    public Author(String name) {
        this.name = name;
    }

    public Author() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
