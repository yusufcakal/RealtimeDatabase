package yusufcakal.realtime_database.service;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.ArrayList;
import java.util.List;
import yusufcakal.realtime_database.pojo.Author;
import yusufcakal.realtime_database.pojo.Book;

/**
 * Created by Yusuf on 18.01.2018.
 */

public class FirebaseUtil {

    public static void saveData(){

        List<Author> authorList = new ArrayList<>();
        Author author1 = new Author("Robert C. Martin");
        authorList.add(author1);
        Book book = new Book("Clean Code", authorList);
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        DatabaseReference databaseBook = database.child("book");
        databaseBook.push().setValue(book);

    }

}
